import { GraphQLFloat, GraphQLID, GraphQLString } from "graphql";
import { ProductType } from "../TypeDefs/Product";
import { MessageType } from "../TypeDefs/Messages";
import { Products } from "../../Entities/Products";
import { Customers } from "../../Entities/Customers";

const YEARLY = 'YEARLY';
const HALFYEARLY = 'HALFYEARLY';
const QUARTERLY = 'QUARTERLY';
const MONTHLY = 'MONTHLY';

export const GET_PRODUCT = {
  type: ProductType,
  args: {
    name: {type: GraphQLString},
    gender: {type: GraphQLString},
    dateOfBirth: {type: GraphQLString},
    paymentTerm: {type: GraphQLString},
    planCode: {type: GraphQLString},
    premium: {type: GraphQLString},
  },
  async resolve(parent: any, args: any) {
    const { name, gender, dateOfBirth, planCode, premiumPerYear, paymentTerm } = args;
    const firstName = name.split(' ')[0];
    const surName = name.split(' ')[1] || '';
    await Customers.insert({ first_name: firstName, sur_name: surName, date_of_birth: dateOfBirth, gender});
    const productQuery = await Products.find({where: {plan_code: planCode}})
    if(productQuery.length === 0){
      return null;
    }
    const result = productQuery[0];
    let total = result.premium;
    if(paymentTerm === YEARLY)
      total = Number(result.premium) * 12;
    if(paymentTerm === HALFYEARLY)
      total = Number(result.premium) * 6;
    if(paymentTerm === QUARTERLY)
      total = (result.premium) * 4;
    result.premium = total;
    return {...result, paymentTerm, name, gender, };
    
  },
}