import { GraphQLFloat, GraphQLID, GraphQLString } from "graphql";
import { CustomerType } from "../TypeDefs/Customer";
import { MessageType } from "../TypeDefs/Messages";
import { Customers } from "../../Entities/Customers";

export const CREATE_CUSTOMER = {
  type: CustomerType,
  args: {
    name: {type: GraphQLString},
    dob: {type: GraphQLString},
    gender: {type: GraphQLString},
  },
  async resolve(parent: any, args: any) {
    const { name, dob, gender } = args;
    const firstName = name.split(' ')[0];
    const surName = name.split(' ')[1] || '';
    await Customers.insert({ first_name: firstName, sur_name: surName, date_of_birth: dob, gender});
    args.firstName = firstName;
    args.surName = surName;
    return args;
  },
};