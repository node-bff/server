import { GraphQLObjectType, GraphQLID, GraphQLString } from "graphql";

export const CustomerType = new GraphQLObjectType({
  name: "Customer",
  fields: () => ({
    id: { type: GraphQLID },
    firstName: { type: GraphQLString },
    surName: { type: GraphQLString },
    gender: { type: GraphQLString },
    dateOfBirth: { type: GraphQLString },
  }),
});
