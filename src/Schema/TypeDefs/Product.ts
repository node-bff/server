import { GraphQLObjectType, GraphQLID, GraphQLString, GraphQLFloat } from "graphql";

export const ProductType = new GraphQLObjectType({
  name: "Product",
  fields: () => ({
    planCode: { type: GraphQLString },
    benifit: { type: GraphQLString },
    premium: { type: GraphQLString },
    name: {type: GraphQLString},
    paymentTerm: {type: GraphQLString},
    gender: {type: GraphQLString},
  }),
});
