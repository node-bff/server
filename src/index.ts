import express from "express";
import { graphqlHTTP } from "express-graphql";
import { schema } from "./Schema";
import dotenv from "dotenv";
import cors from "cors";
import { createConnection } from "typeorm";
import { Users } from "./Entities/Users";
import { Products } from "./Entities/Products";
import { Customers } from "./Entities/Customers";

dotenv.config();
const main = async () => {
  await createConnection({
    type: "mysql",
    host: process.env.DB_HOST,
    port: 3306,
    database: process.env.DB_NAME,
    username: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    logging: true,
    synchronize: false,
    entities: [Users, Products, Customers],
  });

  const app = express();
  app.use(cors());
  app.use(express.json());
  app.use(
    "/graphql",
    graphqlHTTP({
      schema,
      graphiql: true,
    })
  );

  app.listen(3001, () => {
    console.log("SERVER RUNNING ON PORT 3001");
  });
};

main().catch((err) => {
  console.log(err);
});
