import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Products extends BaseEntity {
  @PrimaryGeneratedColumn()
  id!: number;

  @Column()
  plan_code!: string;

  @Column()
  premium!: number;
  
  @Column()
  benifit!: string;

}
